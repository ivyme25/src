﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DemoProject.Models
{
        [MetadataType(typeof(NewsMetadata))]
        public partial class News
        {
        }

        public class NewsMetadata
        {
            [DisplayName("標題")]
            [Required]
            [StringLength(500)]
            public string Title { get; set; }

            [DisplayName("內容")]
            [Required]
            [StringLength(500)]
            public string Content { get; set; }

            [DisplayName("建立時間")]
            [Required]
            public System.DateTime CreateTime { get; set; }
            //將News.cs的程式碼複製到這邊
        } 

}